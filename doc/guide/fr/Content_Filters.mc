Filtres de contenu
==================


### Vue d'ensemble

L'application **Content Filter** (lorsqu'elle est installée) vous permet de filtrer le contenu entrant provenant de l'ensemble des sources ou de connexions spécifiques. Le filtrage peut être basé sur des mots, des hashtags, des expressions régulières ou la langue.

Vous pouvez définir des filtres globaux pour tous les contenus entrants sur la page de l'application Filtre de contenu.

Vous pouvez également définir des filtres individuels pour chacune de vos connexions. Pour cela, sélectionnez "Modifier" pour une connexion, puis "Filtrer" dans le menu déroulant.

Si vous ajoutez des filtres dans la section **Ne pas importer les publications comprenant ce texte**, ils seront appliqués en premier. Tout contenu correspondant sera supprimé.

Ensuite, tous les filtres dans la section **N'importer que les publications comprenant ce texte** seront appliqués. Seul le contenu correspondant sera conservé, et tout ce qui ne correspond pas sera supprimé.


### Filtres de base

**TEXTE**

**Tout texte ne commençant pas par l'un des éléments suivants: lang= lang!= # $ ? /**
Correspondance de texte insensible à la casse
Exemple: `mot` ("mot", "MOT", "Mots", "motiver", "marmots", etc. correspondent)
Exemple: `des pas` (matches "des pas", "des pastèques", "grandes passions", etc. correspondent)
NOTE: Attention à ne pas utiliser de séquences de caractères trop courtes qui correspondent à de nombreux mots différents. Vous pouvez utiliser des EXPRESSIONS REGULIERES (voir ci-dessous).


**LANGUE**

**lang=**
Cible la langue (si elle peut être identifiée)
Exemple: `lang=fr` (conserve le contenu en langue française)

**lang!=**
Exclut la langue (si elle peut être identifiée)
Exemple: `lang!=en` (supprime le contenu n'utilisant pas la langue anglaise)


**HASHTAG**

**#**
Filtre par hashtag
Exemple: `#chatons`


**CATÉGORIE**

**$**
Cible une catégorie - Les catégories sont assimilables à des "hashtags personnels" qui ne s'appliquent qu'à un seul canal. Très peu de plateformes du Fediverse ont un support des catégories. Vous pouvez les utiliser pour votre canal en installant l'application **Catégories**.)
Exemple: `$Science`


**ITEMS ET CHAMPS**

**?**
Voir plus bas, dans **Filtres avancés**


**EXPRESSIONS RÉGULIÈRES (REGEX)**

**/**
Corrrespond à une "expression régulière". De nombreux sites d'aide en ligne tels que [Regular-Expressions.info](https://www.regular-expressions.info/), [regexr.com](https://regexr.com/) ou encore [regexlearn.com](https://regexlearn.com/fr) peuvent vous aider à en apprendre plus sur les expressions régulières.
Exemple: `/Dupon[dt]/` ("Dupond" et "Dupont" correspondent)
Exemple: `/\b[Pp]ont\b/` (les mots "Pont" et "pont" correspondent, mais pas "ponts", "pontage", "apponter", etc.)


### Filtres avancés

**ITEMS**

**?**
Vous pouvez effectuer une recherche de chaîne de caractères, de nombre, de tableau ou de booléens correspondant aux champs de la base de données d'un item (article, commentaire, etc.). Une liste complète dépasse le cadre de ce document, mais vous pouvez consulter le fichier `install/schema_mysql.sql` et rechercher `CREATE TABLE IF NOT EXISTS [nomd]`[/nomd]item[nomd]`[/nomd]`. Quelques exemples:
* `body` (texte de la publication)
* `verb` (ce que fait cet item)
* `item_thread_top` (première publication d'une conversation, booléen)
* `item_private` (0 = publication publique, 1 = publication restreinte, 2 = message direct)
* ...etc...

Les opérateurs de comparaison disponibles sont:
* `?foo ~= baz` -- item.foo contient la chaîne de caractères 'baz'
* `?foo == baz` -- item.foo est la chaîne de caractères 'baz'
* `?foo != baz` -- item.foo n'est pas la chaîne de caractères 'baz'
* `?foo >= 3` -- item.foo est supérieur ou égal à 3
* `?foo > 3` -- item.foo est supérieur à 3
* `?foo <= 3` -- item.foo est inférieur ou égal à 3
* `?foo < 3` -- item.foo est inférieur à 3
* `?foo {} baz` -- 'baz' est un élément de tableau dans item.foo
* `?foo {\*} baz` -- 'baz' est une clé de tableau dans item.foo
* `?foo` -- condition vraie pour item.foo
* `?!foo` -- condition fausse pour item.foo (Les valeurs 0, '', un tableau vide, et une valeur non définie seront évaluées à false)

Exemple: `?verb == Announce` (un "boost" ActivityPub correspond)


**CHAMPS**

**?+**
Champ ciblé: ?+champ item.object
*Utilisation non documentée pour le moment*
