<?php
namespace Code\Lib;

use StephenHill\Base58;

class Multibase
{

    protected $key = null;

    public function __construct()
    {
        return $this;
    }

    public function publicKey($key)
    {
        $base58 = new Base58();
        $raw = hex2bin('ed01') . sodium_base642bin($key, SODIUM_BASE64_VARIANT_ORIGINAL_NO_PADDING);
        return 'z' . $base58->encode($raw);
    }

    public function secretKey($key)
    {
        $base58 = new Base58();
        $raw = hex2bin('8026') . sodium_base642bin($key, SODIUM_BASE64_VARIANT_ORIGINAL_NO_PADDING);
        return 'z' . $base58->encode($raw);
    }

    public function decode($key)
    {
        $base58 = new Base58();
        $key = substr($key,1);
        $raw = $base58->decode($key);
        return sodium_bin2base64(substr($raw, 2), SODIUM_BASE64_VARIANT_ORIGINAL_NO_PADDING);
    }

}
