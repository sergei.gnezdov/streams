<?php

namespace Code\ActivityStreams;

class Link
{
    public $href;
    public $rel;
    public $mediaType;
    public $name;
    public $hreflang;
    public $height;
    public $width;
    public $preview;


    public function __construct($input = null, $strict = false)
    {
        if (isset($input)) {
            if (is_string($input)) {
                $this->string = $input;
            }
            elseif(is_array($input)) {
                foreach ($input as $key => $value) {
                    $key = ($key === '@context') ? 'ldcontext' : $key;
                    if ($strict && !property_exists($this, $key)) {
                        throw new UnhandledElementException("Unhandled element: $key");
                    }
                    $this->{$key} = $value;
                }
            }
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHref()
    {
        return $this->href;
    }

    /**
     * @param mixed $href
     * @return Link
     */
    public function setHref($href)
    {
        $this->href = $href;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRel()
    {
        return $this->rel;
    }

    /**
     * @param mixed $rel
     * @return Link
     */
    public function setRel($rel)
    {
        $this->rel = $rel;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMediaType()
    {
        return $this->mediaType;
    }

    /**
     * @param mixed $mediaType
     * @return Link
     */
    public function setMediaType($mediaType)
    {
        $this->mediaType = $mediaType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return Link
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHreflang()
    {
        return $this->hreflang;
    }

    /**
     * @param mixed $hreflang
     * @return Link
     */
    public function setHreflang($hreflang)
    {
        $this->hreflang = $hreflang;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $height
     * @return Link
     */
    public function setHeight($height)
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $width
     * @return Link
     */
    public function setWidth($width)
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPreview()
    {
        return $this->preview;
    }

    /**
     * @param mixed $preview
     * @return Link
     */
    public function setPreview($preview)
    {
        $this->preview = $preview;
        return $this;
    }

    public function toArray()
    {
        if ($this->string) {
            return $this->string;
        }
        $returnValue = [];
        foreach ((array) $this as $key => $value) {
            if (isset($value)) {
                $key = ($key === 'ldcontext') ? '@context' : $key;
                if ($value instanceof ASObject || $value instanceof Link) {
                    $returnValue[$key] = $value->toArray();
                }
                else {
                    $returnValue[$key] = $value;
                }
            }
        }
        return $returnValue;
    }


}
