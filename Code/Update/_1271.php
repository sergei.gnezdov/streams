<?php
namespace Code\Update;

class _1271
{
    public function run()
    {

        q("START TRANSACTION");

        if (ACTIVE_DBTYPE == DBTYPE_POSTGRES) {
            $r1 = q("ALTER TABLE channel ADD channel_epubkey text");
            $r2 = q("ALTER TABLE channel ADD channel_eprvkey text");

            $r3 = q("ALTER TABLE channel ALTER channel_epubkey set NOT NULL");
            $r4 = q("ALTER TABLE channel ALTER channel_eprvkey set NOT NULL");

            $r = $r1 && $r2 && $r3 && $r4;
        }
        else {
            $r1 = q("ALTER TABLE channel ADD channel_epubkey text NOT NULL");
            $r2 = q("ALTER TABLE channel ADD channel_eprvkey text NOT NULL");
    
            $r = $r1 && $r2;
        }
    
        $r3 = q("select channel_id from channel where true");
        if ($r3) {
            foreach ($r3 as $channel) {
                $keys = sodium_crypto_sign_keypair();
                $pubkey = sodium_bin2base64(sodium_crypto_sign_publickey($keys),SODIUM_BASE64_VARIANT_ORIGINAL_NO_PADDING);
                $prvkey = sodium_bin2base64(sodium_crypto_sign_secretkey($keys),SODIUM_BASE64_VARIANT_ORIGINAL_NO_PADDING);
                q("update channel set channel_epubkey = '%s', channel_eprvkey = '%s' where channel_id = %d",
                    dbesc($pubkey),
                    dbesc($prvkey),
                    intval($channel['channel_id'])
                );
            }
        }

        if ($r) {
            q("COMMIT");
            return UPDATE_SUCCESS;
        }

        q("ROLLBACK");
        return UPDATE_FAILED;
    }

    public function verify()
    {
        $columns = db_columns('channel');
        return in_array('epubkey', $columns) && in_array('eprvkey', $columns);
    }
}

